# project2

continuous delivery of mini project 4
gitlab link: https://gitlab.com/Angelina77/myactixweb
This project repo only contains README.md file that explains the steps I updated from mini project 4.
The actual project is in the previous gitlab repo https://gitlab.com/Angelina77/myactixweb.

## add POST endpoint to the actix-web app
```rust
struct Echo {
    message: String,
}

// The handler function for the POST request at "/echo"
// It accepts a JSON object, deserializes it, and then echoes it back.
async fn echo(echo: web::Json<Echo>) -> Result<impl Responder> {
    Ok(HttpResponse::Ok().json(Echo {
        message: echo.message.clone(),
    }))
}
```

test the POST endpoint in the actix-web app
use the following command to test the POST endpoint:
```bash
curl -X POST -H "Content-Type: application/json" -d '{"message":"Test, Actix!"}' http://localhost:8080/echo
```

![post](echo.png)

## Add CI/CD to your project

- add a `.gitlab-ci.yml` file:

```yaml
variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

services:
  - docker:dind

stages:
  - build
  - test
  - dockerize

build:
  image: rust:latest
  stage: build
  script:
    - rustup default stable
    - rustc -V
    - cargo -V
    - cargo build --verbose

test:
  image: rust:latest
  stage: test
  script:
    - cargo test --verbose

dockerize:
  image: docker:25.0.3
  stage: dockerize
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
  only:
    - main
```
- add a `Dockerfile`:

```Dockerfile
# Build the app
FROM rust:1.75 as builder
WORKDIR /usr/src/actix_web_app
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# This dummy build helps to cache the dependencies which will not change often
RUN mkdir src && \
    echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs && \
    cargo build --release && \
    rm -f target/release/deps/actix_web_app*

# Now copy in the actual source code and do the final build
COPY ./src ./src
RUN cargo build --release

# Build the final image
FROM debian:bookworm-slim
COPY --from=builder /usr/src/actix_web_app/target/release/actix_web_app /usr/local/bin

# Run the app
CMD ["actix_web_app"]
```
Go to gitlab project's settings > CI/CD and add a new variable `DOCKERHUB_USERNAME` and `DOCKERHUB_PASSWORD` with my dockerhub credentials.

successfull build of the project in gitlab ci/cd pipeline:
![gitlab-ci](gitlab-ci.png)

successful push of the docker image to gitlab registry:
![gitlab-registry](container.png)



